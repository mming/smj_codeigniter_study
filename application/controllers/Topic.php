<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Topic
 * 무조건 대문자 써야 함
 */
class Topic extends CI_Controller {

	/**
	 *  thdalswl02.cafe24.com/index.php/topic/get
	 */
	public function index()
	{
		$this->load->view('head');
		$this->load->view('topic');
		$this->load->view('footer');
	}

	/**
	 * @param $id
	 * http://thdalswl02.cafe24.com/index.php/topic/get/2
	 */
	public function main($id) {
		$this->load->view('head');
		// get/$id
		$data = array();
		$data['id'] = $id;
		$this->load->view('main', $data);
		$this->load->view('footer');


	}
}
